# MUonE_DAQ

Software for MUonE DAQ decoding 


# INSTRUCTIONS

To run the decoding script and the monitoring script you must create a conda environment and install the `nlohmann_json` package first.
The decoding script must then be compiled according to the instructions below.

# HOW TO SET UP CONDA AND ROOT

If you're working on lxplus, please go to the lxplus section.

If you do not have it already installed, you should download and install either the Anaconda or the Miniconda packages.

https://docs.anaconda.com/anaconda/install/index.html

https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/index.html

The source file `setconda.sh` creates a conda environment named muone_daq, then proceeds to install the `nlohmann_json` package which is required to read json files in ROOT.
Please notice that the variable `ROOT_INCLUDE_PATH` should point to the /include/ directory where this package has been installed. This should correspond to the path within the conda environment that you have just created and activated. For instance, after running `source setconda.sh`, the output of
```
echo $ROOT_INCLUDE_PATH
```
should be somewhat similar to
```
/home/capriotl/miniconda3/envs/muone_daq/include/
```
Keep in mind that this will vary depending on your OS, your local configurations and the Anaconda/Miniconda version.


# HOW TO SET UP CONDA AND ROOT IN LXPLUS

Conda is already installed in lxplus, so it only needs to be activated and cofigured. As easy as it seems,

```
source setconda_lxplus.sh
```
will configure the conda environment and install the relevant packages. The default python version is 3.9; if an older version is required, you can modify the source file and change the option `python=3.6` with the desired version.

# COMPILING            

To compile the decode script you must use the following command within the conda environment:
```
g++ -I$CONDA_PREFIX/include/ decode_tojson.cpp -o decode_tojson
```
In some cases, depending on the native compiler, it might be necessary to specify ```std=c++11``` in the compilation command.
Please notice that the additional path corresponds to the library where the nlohmann_json package is installed within the conda environment. The `$CONDA_PREFIX` variable should point to the path of the environment activated in the previous step, i.e. the path to the directory where the `nlohmann_json` package has been installed.


# COMPILING IN LXPLUS

Just like above:
```
g++ -I$CONDA_PREFIX/include/ decode_tojson.cpp -o decode_tojson -std=c++11
```
Notice that in lxplus you must specify the flag ```std=c++11```.

# RUNNING THE DECODE ON LXPLUS

The first thing to do is to setup conda:
``` 
source setconda_lxplus.sh 
```

Then one must compile the decode script:
```
g++ -I$CONDA_PREFIX/include/ decode_tojson.cpp -o decode_tojson ```std=c++11```
```

To run the decode script you must run:
```
./decode_tojson [inputfile.dat] [outputfile.json]
```

To run the monitoring script you must run:
```
root -l
.L jsonToRoot.cpp
jsonToRoot("name_of_json_file.json")
```


//#include "decode_functions.h"
#include "xml_reading_utils.h"

const unsigned HEADER = 0xaa;

//const int NMODULES = 10;
//const int NSTATIONS = 1;
int nEvents = 0;

// nlohmann json @: https://github.com/nlohmann/json 

/*
 Run as : ./decode_tojson <input file> <output file>

 File format in 64b words :
  header 0
  payload 0 : word 0
  ...
  payload 0 : word N-1
  zeros     : word 0
  ...
  zeros     : word M-1
  header 1
  payload 1 : word 0
  ...

 Header is a 64b word, format in 4b nibbles is :
  A A L L | R R R S | S S S S | S S S S
  -------------------------------------
 Where :
     b63-b56 : 8b header code 0xAA
     b55-b48 : 8b payload length in terms of 64b words
     b47-b36 : 12b run number
     b35-b00 : 36b packet number
*/



int main(int argc, char** argv)
{
    const char * fname = argv[1]; 
    FILE * f = fopen(fname, "rb");
    
    const char * nfout = argv[2]; // out full name; 
    ofstream fout;
    cout << "saving in file: " << nfout << endl;
    //fout.open(nfout, ios::out);
    
    const char * nxml = argv[3]; // xml settings file

    char buf[8]; //64b --> 1 char is 8b
    
    bool firsttime = true;
    unsigned long seq_prev;
    int bad_seq_cnt = 0;

    //allocate memory for objects of the classes Station and Module, reading them from xml file

    Station stations[NSTATIONS];
    readXml(nxml, stations);
    for(int i = 0; i<NSTATIONS; i++) stations[i].printStation();


    while ( !(feof(f)) )
    {
        fread(buf,1,8,f); // 8x1B = 64b -- reads 64 bits and moves the pointer for the next iteration

        unsigned code = (unsigned)(buf[7])&0xff;

        headerWord          header;
        
        if( code == 0xaa ) { // aa = 10 10 10 10

            unsigned pktlen     = getPktLengthHeader(buf); 
            unsigned run        = getRunHeader(buf);
            unsigned long seq   = getSeqHeader(buf);

            //printf( "\thdrcode  : 0x%2x\n",      code);
            //printf( "\tpktlen   : 0x%2x\n",      pktlen);
            //printf( "\trun#     : 0x%3x\n",      run);
            //printf( "\tseq#     : 0x%09llx\n",   seq);
            
            header.addLinkHeader(pktlen, run, seq);

            // check sequence
            if( !firsttime )
            {
                if( seq != seq_prev+1 )
                {
                printf( "ERROR :: bad sequence : 0x%09llx vs expected 0x%09llx\n", seq, seq_prev+1);
                bad_seq_cnt++;
                }
            }

            seq_prev = seq;

            //printf("\n   -- data start -- \n");

            vector<stubWord>    pktStubWords;

            for( int wrdrd=0; wrdrd<pktlen; wrdrd++ )
            {
                fread(buf,1,8,f);
                //first two words are the payload headers. TODO: Structure of the packet changed!!! update
                if(wrdrd == 0)
                {
                    unsigned BxSuperId = *((unsigned *)(&(buf[0])));
                    unsigned IPbus_user_data = *((unsigned *)(&(buf[4])));
                    header.addPayload1(BxSuperId, IPbus_user_data);
                }

                else if (wrdrd == 1)
                {
                    int16_t ModuleBits[4];
                    for(int i=0; i<4; i++) ModuleBits[i] = *((int16_t *)(&(buf[i*2])));
                    header.addPayload2(ModuleBits);
                }

                else
                {
                    char stub[2][4]; 
                    //64b word contains 2 32b stubs --> all the functions to extract physical values act on one 32b word 
                    //--> easier to live with 2 32b words
                    // if performance/memory is a problem we can just manage to use only one variable, rearranging either buf or stub
                    for(int i=0; i<4; i++)  stub[0][i] = buf[i]&0xff;
                    for(int i=4; i<8; i++)  stub[1][i-4] = buf[i]&0xff;

                    stubWord firstWord(stub[0]);
                    stubWord secondWord(stub[1]);
                    
                    if(firstWord.isGood())  pktStubWords.push_back(firstWord); //if time is a problem, we can delete this naive quality check
                    if(secondWord.isGood()) pktStubWords.push_back(secondWord);
                }
            }
            
            //now let's reorganize the stubs in Modules and in Events
            //Module    allModules[NMODULES];
            //Station   allStations[NSTATIONS];

            int bx = (*pktStubWords.begin()).Bx;

            for(auto i = 0; i < pktStubWords.size(); i++)
            {
                //Save event if the bx is changing OR if we are at the last place in the vector
                if(bx!= pktStubWords[i].Bx | i == pktStubWords.size() - 1)
                {
                    Event CurrentEvent;

                    //TODO: maybe need to implement a smarter way to initialize the Event                        
                    CurrentEvent.BxId = bx;
                    CurrentEvent.setStations(stations);
                    //CurrentEvent.add_station(allStations[0]);
                    CurrentEvent.addHeaderWord(header);

                    nlohmann::json j;
                    nlohmann::json cerealOutput = CurrentEvent.jsonOut();
                    
                    fout.open(nfout, ios::app);
                    fout << cerealOutput << endl;
                    fout.close();
                    
                    CurrentEvent.printEvent();
                    //Clear objects used to store info
                    //for(int i=0; i<NMODULES_PERSTATION; i++)   allModules[i].clearModule();
                    for(int i=0; i<NSTATIONS; i++)  stations[i].clearStation();
                    bx = pktStubWords[i].Bx;
                    
                    
                    nEvents++;
                }

                firsttime = false;

                int whichModule   = pktStubWords[i].getModule();
                int whichStation  = pktStubWords[i].getStation();

                stations[whichStation].add_stub_from_stubword(pktStubWords[i], whichModule);


            }
            // now return to main read loop, look again for the hdr code ...
        }

        
        //fout.close();

    } //while !eof

    
    cout << "Saved: "<< nEvents << " events " << endl;
    cout << "Sequence errors: " << bad_seq_cnt << endl;

}

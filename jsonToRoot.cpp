#include "xml_reading_utils.h"
#include <string>
#include <sstream>

using namespace nlohmann;

int jsonToRoot(string fileName, string nxml)
{
    std::ifstream ifs(fileName);
    
    int evtnum = 0;
    std::string line;

    //cout << ifs.is_open() << endl;
    

    Station stations[NSTATIONS];
    readXml(nxml, stations);

    while (std::getline(ifs, line))
    {
        for(int i=0; i<NSTATIONS; i++) stations[i].clearStation();

        if(evtnum%10==0) 
        {
            cout << "!------- Reading event number " << evtnum << " -------!" << endl;
            std::istringstream iss(line);
            json jf = json::parse(iss);

            //Set event object
            Event ev;
            ev.setStations(stations);   //set stations in the event with the info read from xml
            ev.getInfoFromJson(jf);     //gets actual info from json
            ev.getStation(0).getModule(0).printModule();
            //ev.printEvent();
            evtnum++;
        }
    }
    

    return 1;
}


int main()
{
    jsonToRoot("provlone.json", "xmltest.xml");
    return 1;
}
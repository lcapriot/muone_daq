#include <stdio.h>
#include <iostream>
#include <stdbool.h>
#include <string.h>
#include <vector>
#include <math.h>
#include <fstream>
#include <nlohmann/json.hpp>
#include "constants.h"



using namespace std;

////////////////////////////////////////////////////////////////
/////////////////////////  UTILITIES  //////////////////////////
////////////////////////////////////////////////////////////////

//functions for transforming the 32 and 64 bit words in useful quantities
//names should be self-explaining, btw the "Header" functions extract info
//from headers, and the "32b" functions extract info from the 32 bit stub 
//words

unsigned getPktLengthHeader(char buf[8])
{
    unsigned pktlen = buf[6]&0xff;
    return pktlen;
}

unsigned getRunHeader(char buf[8])
{
    unsigned run = (((unsigned)(buf[5])) << 4)&0xff0  | ((unsigned)(buf[4])>>4)&0xf; // run number, the << >> shift bits left/right by the successive number of bites
    return run;
}

unsigned long getSeqHeader(char buf[8])
{
    unsigned long seq = ((unsigned long)buf[0])&0xff | ((unsigned long)(buf[1])<<8)&0xff00 | ((unsigned long)(buf[2])<<16)&0xff0000 | ((unsigned long)(buf[3])<<24)&0xff000000 | ((unsigned long)(buf[4])<<32)&0xf00000000 ;
    return seq;
}

int getBend_32b(char buf[4])
{   
    int bend = (int)(buf[0]&0xf);
    return bend;
}

int getAddress_32b(char buf[4])
{
    int address = (int) ( (buf[1]<<4)&0xf0 | (buf[0]>>4)&0xf );
    return address;
}

int getCBCid_32b(char buf[4])
{
    int CBCId = (int) (buf[1]>>4&0x7);
    return CBCId;
}

int getBx_32b(char buf[4])
{
    char mostBx = (((buf[3]<<1)&0xE | buf[2]>>7 &0x1));
    char midBx  = (buf[2] >> 3) &0xf;
    char lastBx = (((buf[2]<<1)&0xE | (buf[1]>>7)&0x1));
    char16_t allTogether = ((mostBx << 8)&0xf00 | (midBx)<<4&0xf0 | lastBx&0xf);
    return (int) allTogether;
}

int getCICid_32b(char buf[4])
{
    int CICId = (int) (buf[3]>>3)&0x1;
    return CICId;
}

int getLinkid_32b(char buf[4])
{
    int linkId = (int) (buf[3]>>4)&0xf;
    return linkId;
}

void printWord(char buf[8])
{
    printf("%02x%02x %02x%02x %02x%02x %02x%02x \n\n",
              0xff&buf[7], 0xff&buf[6],
              0xff&buf[5], 0xff&buf[4],
              0xff&buf[3], 0xff&buf[2],
              0xff&buf[1], 0xff&buf[0]);
}

unsigned long conversion64bWordToUnsigned(char buf[8])
{
    unsigned long val_u64 = 0;
    for( int i=0; i<8; i++ )  val_u64 |= ((unsigned long)(buf[i])&0xff)<<(i*8ULL);
    return val_u64;
}

unsigned long conversion64bWordToUnsigned_cast(char buf[8])
{
    unsigned long cast_u64;
    cast_u64 = *((unsigned long *)(&(buf[0])));
    return cast_u64;
}

//Read XML function

////////////////////////////////////////////////////////////////
/////////////////////////   CLASSES   //////////////////////////
////////////////////////////////////////////////////////////////

//All the classes information, the purpose of the classes is to 
//mimic the real life objects, so we have an "Event", in each
//Event we have N Stations, in each Station six Modules and in
//each Module N' stubs.
//The headerWord(stubWord) classes are used to cleanly store
//information from the 64(32) bit words with header(stub) info
//and manage them more easily before putting them in the 
//Event-Station-Stub-Module objects.

class headerWord
{
    private:

        unsigned        packetLength;
        unsigned        run;
        unsigned long   sequence;
        unsigned        BxSuperId;
        unsigned        IPbus_user_data;
        int16_t         moduleStatusBits[4];

    public:

        headerWord()
        {
            BxSuperId       = -999;
            IPbus_user_data = -999;
            for(int i=0; i<4; i++)    moduleStatusBits[i] = -999; 
        }

        headerWord(unsigned Bx, unsigned Ipbus, int16_t moduleStatus[4])
        {
            BxSuperId = Bx;
            IPbus_user_data = Ipbus;
            for(int i=0; i<4; i++)    moduleStatusBits[i] = moduleStatus[i]; 
        }

        headerWord(nlohmann::json j) //constructor from json
        {
            packetLength            = j["packetLength"];
            run                     = j["run"];
            sequence                = j["sequence"];
            BxSuperId               = j["BxSuperId"];
            IPbus_user_data         = j["IPbus_user_data"];
            for(int i=0; i<4; i++)  moduleStatusBits[i] = j["moduleStatusBits"][i];
        }


        void addLinkHeader(unsigned pktlen, unsigned Run, unsigned long seq)
        {
            packetLength    = pktlen;
            run             = Run;
            sequence        = seq;
        }

        void addPayload1(unsigned Bx, unsigned Ipbus)
        {
            BxSuperId = Bx;
            IPbus_user_data = Ipbus;
        }
        
        void addPayload2(int16_t moduleStatus[4])
        {
            for(int i=0; i<4; i++)    moduleStatusBits[i] = moduleStatus[i]; 
        }

        nlohmann::json jsonOut() //output in the json format for the header
        {
            nlohmann::json oats;
            oats["packetLength"]        = packetLength;
            oats["run"]                 = run;
            oats["BxSuperId"]           = BxSuperId;
            oats["sequence"]            = sequence;
            oats["IPbus_user_data"]     = IPbus_user_data;
            oats["moduleStatusBits"]    = moduleStatusBits;

            return oats;
        }

        //string writeHeaderWord

};

class stubWord
{
    private:

    public:

        int bend;
        int address;
        int CBCid;
        int Bx;
        int CICid;
        int Linkid;

        stubWord(char buf[4])
        {
            bend    = getBend_32b(buf);
            address = getAddress_32b(buf);
            CBCid   = getCBCid_32b(buf);
            Bx      = getBx_32b(buf);
            CICid   = getCICid_32b(buf);
            Linkid  = getLinkid_32b(buf);
        }

        void translateBit() //print out meaningful wuantities from bits
        {
            cout << endl << "Translating the word, you obtain: " << endl 
            << "bend     " << bend << endl
            << "address  " << address << endl
            << "CBCid    " << CBCid << endl
            << "Bx       " << Bx << endl
            << "CICid    " << CICid << endl
            << "Linkid   " << Linkid << endl<< endl;
        }

        int isGood() //easy check that all the quantities are inside their range
        {
            if(bend<16 && address>0 && address<255 && CBCid<9 && Bx<4096 && CICid<2 && Linkid<16 ) return 1;
            return 0;
        };

        int getModule()
        {
            return Linkid; //TODO to be updated when we can actually understand what is the module from the linkid
        }

        int getStation()
        {
            return Linkid/NMODULES_PERSTATION; //TODO to be updated when we can actually understand what is the module from the linkid
        }
};

class Stub
{
    private:

        int bend;
        int address;
        int CBCid;
        int CICid;

    public:

        Stub()
        {
            bend    = -999;
            address = -999;
            CBCid   = -999;
            CICid   = -999;
        }

        Stub(stubWord b)
        {
            bend    = b.bend;
            address = b.address;
            CBCid   = b.CBCid;
            CICid   = b.CICid;
        };

        Stub(int ben, int add, int cbc, int cic)
        {
            bend    = ben;
            address = add;
            CBCid   = cbc;
            CICid   = cic;
        };

        Stub(nlohmann::json j)
        {
            bend    = j["bend"];
            address = j["address"];
            CBCid   = j["CBCid"];
            CICid   = j["CICid"];
        };

        void printStub()
        {
            cout << "bend    " << bend << endl
            << "address " << address << endl
            << "CBCid   "<< CBCid << endl
            << "CICid   " << CICid << endl; 
        };

        double getLocalX()
        {
            const double MAXX    = 7*255;
            double localX   = (CBCid+1)*address*1.0/MAXX;
            return localX;
        }

        double getLocalY()
        {
            //returns the center of the half module related to the CICid
            double localY = 0.5/2 + CICid/2.;
            return localY;
        }

        int getBend()
        {
            return bend;
        }

        int getAddress()
        {
            return address;
        }

        int getCBCid()
        {
            return CBCid;
        }

        int getCICid()
        {
            return CICid;
        }

        /*double GetAbsoluteX(Module m)
        {

        }
        
        double GetAbsoluteY(Module m)
        {

        }*/
        

        string stubStringOut()
        {
            string s = to_string(bend) + " , " + to_string(address) + " , " + to_string(CBCid) + " , " + to_string(CICid);
            return s;
        }

        nlohmann::json jsonOut()
        {   
            nlohmann::json almond;
            
            almond["bend"]      = bend;
            almond["address"]   = address;
            almond["CBCid"]     = CBCid;
            almond["CICid"]     = CICid;
            return almond;
        }


};

class Module
{
    private:
        vector<Stub>    stubs;
        int             isOn;
        double          positionX;
        double          positionY;
        double          positionZ;
        double          rotationX; //rotation angle in radiants -- All info for modules and stations must be implemented in an input file
        double          rotationY; //rotation angle in radiants -- All info for modules and stations must be implemented in an input file
        double          rotationZ; //rotation angle in radiants -- All info for modules and stations must be implemented in an input file
        
    public:

        Module()
        {
            
            stubs.clear();
            stubs.push_back(*(new Stub()));
            positionX = -999;
            positionY = -999;
            positionZ = -999;
            rotationX = -999;
            rotationY = -999;
            rotationZ = -999;

        }

        Module(double pX, double pY, double pZ, double rX, double rY, double rZ, int on)
        {
            stubs.clear();
            isOn      = on;
            positionX = pX;
            positionY = pY;
            positionZ = pZ;
            rotationX = rX;
            rotationY = rY;
            rotationZ = rZ;
        }
               void clearModule()
        {
            stubs.clear();
        }

        int isGood()
        {
            if(stubs.size()!=0) return 1;
            return 0;
        }

        void hasStub(Stub st)
        {
            stubs.push_back(st);
        };

        void add_stubword(stubWord s)
        {
            Stub provStub(s);
            stubs.push_back(provStub);
        };

        void printModule()
        {
            cout << "Module has position (x,y,z):   (" << positionX << ", " << positionY << ", " << positionZ << ")" << endl;
            cout << "Module has rotation (x,y,z):   (" << rotationX << ", " << rotationY << ", " << rotationZ << ")" << endl;
            cout << "Module is working? " << isOn << endl;
            for(auto it=stubs.begin(); it!=stubs.end(); it++)
            {
                it->printStub();
                cout << endl;
            }
        }
 
        Module(nlohmann::json j)
        {
            positionX    = j["positionX"];
            positionY    = j["positionY"];
            positionZ    = j["positionZ"];
            rotationX    = j["rotationX"];
            rotationY    = j["rotationY"];
            rotationZ    = j["rotationZ"];
            isOn         = j["isOn"];
            //rotation    = j["rotation"];
            //offset      = j["offset"];

            stubs.clear();
            for(int i=0; i<j["Stubs"].size(); i++)
            {
                Stub s(j["Stubs"][i]);
                stubs.push_back(s);
            }      
            
        }

        void setFromJson(nlohmann::json j)
        {
            positionX    = j["positionX"];
            positionY    = j["positionY"];
            positionZ    = j["positionZ"];
            rotationX    = j["rotationX"];
            rotationY    = j["rotationY"];
            rotationZ    = j["rotationZ"];
            isOn         = j["isOn"];
            //rotation    = j["rotation"];
            //offset      = j["offset"];

            stubs.clear();
            for(int i=0; i<j["Stubs"].size(); i++)
            {
                Stub s(j["Stubs"][i]);
                stubs.push_back(s);
            }      
            
        }


        vector<Stub> getStubs()
        {
            return stubs;
        }


        nlohmann::json jsonOut()
        {
            nlohmann::json strawberry;

            strawberry["positionX"]  = positionX;
            strawberry["positionY"]  = positionY;
            strawberry["positionZ"]  = positionZ;
            strawberry["rotationX"]  = rotationX;
            strawberry["rotationY"]  = rotationY;
            strawberry["rotationZ"]  = rotationZ;
            strawberry["isOn"]       = isOn;
            strawberry["nStubs"]     = stubs.size();

            vector<nlohmann::json> vec;
            for(auto it = stubs.begin(); it!= stubs.end(); it++)    vec.push_back(it->jsonOut());

            strawberry["Stubs"]     = vec;

            return strawberry;
        
        }
};

class Station
{
    private:
        int position;

    public:

        Module modules[NMODULES_PERSTATION]; 
        
        Station()
        {
            for(int i=0; i<NMODULES_PERSTATION; i++) modules[i] = *(new Module());
            position = -999;
        }

        Station(int pos)
        {
            position = pos;
        }

        Station(Module mod0, Module mod1, Module mod2, Module mod3, Module mod4, Module mod5)
        {
            modules[0] = mod0;
            modules[1] = mod1;
            modules[2] = mod2;
            modules[3] = mod3;
            modules[4] = mod4;
            modules[5] = mod5;
        };

        bool SetModuleN(int n, double pX, double pY, double pZ, double rX, double rY, double rZ, int on)
        {
            if(n>NMODULES_PERSTATION && n>=0)
            {
                cout << "Number too high --> impossible to set module" << endl;
                return false;
            }
            else
            {
                modules[n] = *(new Module(pX, pY, pZ, rX, rY, rZ, on));
                return true;
            }
        }

        Station(nlohmann::json j)
        {
            position = j["position"];

            Module mod[NMODULES_PERSTATION];
            for(int i=0; i<NMODULES_PERSTATION; i++)    modules[i]= *(new Module(j["modules"][i]));
        }

        void setFromJson(nlohmann::json j)
        {
            position = j["position"];

            //for(int i=0; i<NMODULES_PERSTATION; i++)    modules[i].setFromJson(j["modules"][i]);
        }

        void setPosition(int i = -999)
        {
            position = i;
        }
/*
        void addModule(Module mod)
        {
            int positionInsideStation = (mod.position/NMODULES_PERSTATION)-position;
            modules[positionInsideStation] = mod;
        }*/

        void clearStation()
        {
            for(int i=0; i<NMODULES_PERSTATION; i++) modules[i].clearModule();
        }

        void printStation()
        {
            cout << "Station "<< position << endl;

            for(int i=0; i<NMODULES_PERSTATION; i++)
            {
                cout << "Module " << i  << endl;
                modules[i].printModule();
            }
            cout << "end station " << endl << endl;
        }

        void add_stub_from_stubword(stubWord s, int whichModule)
        {
            position = 0;
            int positionInsideStation = (whichModule/NMODULES_PERSTATION)-position;
            modules[positionInsideStation].add_stubword(s);
        }

        int getPosition() 
        {
            return position;
        }

        Module getModule( int i = 0 ) 
        {
            //ADD CHECK ON i
            return modules[i];
        }

        Module *getModulesArray( )
        {
            return modules;
        }


        nlohmann::json jsonOut()
        {
            nlohmann::json raspberry;
            raspberry["position"] = position;
            vector<nlohmann::json> vec;

            for(int i=0; i<NMODULES_PERSTATION; i++)    vec.push_back(modules[i].jsonOut());
            raspberry["modules"] = vec;

            return raspberry;
            
        }
};

class Event
{
    private:

        Station stations[NSTATIONS];
        headerWord      headerInfo; 

    public:

        int BxId;

        Event()
        {
            for(int i=0; i<NSTATIONS; i++) stations[i].clearStation();
        };

        Event(nlohmann::json j)
        {
            for(int i=0; i<NSTATIONS; i++) stations[i].clearStation();
            BxId = j["Bx"];
            for(int i=0; i < NSTATIONS; i++)
            {
                stations[i].setFromJson(j["Stations"][i]);
                //stations.push_back(s);
            }

            headerWord h(j["headerInfo"]);

            headerInfo = h;
        }

        void getInfoFromJson(nlohmann::json j)
        {
            BxId = j["Bx"];
            for(int i=0; i < NSTATIONS; i++)
            {
                stations[i].setFromJson(j["Stations"][i]);
                //stations.push_back(s);
            }

            headerWord h(j["headerInfo"]);

            headerInfo = h;
        }
        
        void setStations(Station s[NSTATIONS])
        {
            for(int i=0; i<NSTATIONS; i++)
                stations[i] = s[i];
        }

        void addHeaderWord(headerWord w)
        {
            headerInfo = w;
        }

        void printEvent()
        {
            cout << endl << "----------" << endl << "Event Bx: " << BxId << endl;
            for(int i=0; i<NSTATIONS; i++)
            {
                stations[i].printStation();
            }
            cout << endl << "----------" << endl;
        }

        Station* getStations()
        {
            return stations;
        }

        Station getStation( int i = 0)
        {
            return stations[i];
        }


        headerWord getHeaderWord()
        {
            return headerInfo;
        }

        nlohmann::json jsonOut()
        {
            nlohmann::json berry;
            berry["Bx"] = BxId;
            vector<nlohmann::json> vec;

            for(int i=0; i < NSTATIONS; i++)
            {
                vec.push_back(stations[i].jsonOut());
            }
            berry["Stations"] = vec;
            berry["headerInfo"] = headerInfo.jsonOut();
            
            return berry;
        }

};



conda create --name muone_daq --python=3.6
conda activate muone_daq
conda install -c conda-forge nlohmann_json
conda install -c sbl rapidxml
export ROOT_INCLUDE_PATH=$CONDA_PREFIX/include/

#include "decode_functions.h"
#include "rapidxml.hpp"


void readXml(string xmlname, Station s[NSTATIONS])
{
    //Station s[NSTATIONS];
    rapidxml::xml_document<> doc;
    rapidxml::xml_node<> * root_node = NULL;
    // Read the sample.xml file
    ifstream theFile (xmlname);
    vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
    buffer.push_back('\0');
    
    // Parse the buffer
    doc.parse<0>(&buffer[0]);
    
    // Find out the root node
    root_node = doc.first_node("MUonE_structure");
    
    // Iterate over the student nodes
    for (rapidxml::xml_node<> * station_node = root_node->first_node("Station"); station_node; station_node = station_node->next_sibling())
    {
    
        int s_number = stoi(station_node->first_node("number")->value());
        s[s_number].setPosition(s_number);

        for (rapidxml::xml_node<> * module_node = station_node->first_node("Module"); module_node; module_node = module_node->next_sibling())
        {
            int nModule = stoi(module_node->first_node("number")->value());
            int isON = stoi(module_node->first_node("isOn")->value());

            float pX = atof(module_node->first_node("positionX")->value());
            float pY = atof(module_node->first_node("positionY")->value());
            float pZ = atof(module_node->first_node("positionZ")->value());
            
            float rX = atof(module_node->first_node("rotationX")->value());
            float rY = atof(module_node->first_node("rotationY")->value());
            float rZ = atof(module_node->first_node("rotationZ")->value());

            s[s_number].SetModuleN(nModule, pX, pY, pZ, rX, rY, rZ, isON);
        }
    }
    
    //return s;
}
